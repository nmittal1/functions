#!/usr/bin/env python
# coding: utf-8

# # USER-DEFINED FUNCTION

# In[ ]:


def my_function():
    RETURN 


# In[19]:


def hie(x):
    return x*x


# In[20]:


print(hie(5))


# In[1]:


def my_function():
  print("Hello from a function")


# In[4]:


my_function()


# In[5]:


def my_function(fname):
  print(fname + " Refsnes")

my_function("Emil")
my_function("Tobias")
my_function("Linus")


# # TO ADD 10 WHATEVER VALUE IS PASSED

# In[21]:


def add_10(x) :
    return x+10

print(add_10(10))
    


# In[11]:


def mul_10(x):
 return x*10

print(mul_10(80))


# In[12]:


def find_Evenodd(num):
    number=(num/2)*2
    if(number==num):
        print(num, "is a even number");
    else:
        print(num, "is a odd number");


# In[26]:


print(find_Evenodd(85))


# In[17]:


def odd_even(x):
    y=(x/2)*2
    if(y==x):
        print(x,"even")
    else:
            print(x,"odd")
        


# In[18]:


print(odd_even(83))


# In[ ]:




